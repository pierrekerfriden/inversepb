import numpy as np
from sklearn.gaussian_process.kernels import ConstantKernel, RBF, WhiteKernel
from sklearn.gaussian_process import GaussianProcessRegressor

class DataSet:
    def __init__(self,x,y,FileName):
        self.x = x
        self.y = y 
        self.FileName = FileName

    def SaveData(self):
        np.save('Data/' + self.FileName + '_x.npy', self.x)
        np.save('Data/' + self.FileName + '_y.npy', self.y)

    def ReadData(self):  
        #print('./'+self.FileName + '_x.npy')  
        self.x = np.load('Data/' + self.FileName + '_x.npy',allow_pickle=True)
        self.y = np.load('Data/' + self.FileName + '_y.npy',allow_pickle=True)


class GPRMetaModel: # non-parametric meta-model
    def __init__(self,DataSet):
        self.DataSet = DataSet
        self.x = DataSet.x
        self.y = DataSet.y
        self.gpr = np.ndarray((self.y.shape[1],),dtype=np.object)
        #print('self.y.shape ',self.y.shape) # must be [N,1] if single output. use .reshape([-1,1]) to force the second dimension to 1.
        for i in range(self.y.shape[1]):
            #self.Kernel = ConstantKernel(constant_value=1.0, constant_value_bounds=(0.0, 10.0)) * RBF(length_scale=0.5, length_scale_bounds=(0.0, 10.0)) + RBF(length_scale=2.0, length_scale_bounds=(0.0, 10.0)) 
            dy = 0.
            Kernel = ConstantKernel() * RBF() + WhiteKernel()
            self.gpr[i] = GaussianProcessRegressor(kernel=Kernel, alpha=dy ** 2, random_state=10).fit(self.x, self.y[:,i])
            self.gpr[i].score(self.x, self.y[:,i])
            #print( 'self.gpr[i].kernel_ : ', self.gpr[i].kernel_ )

    def eval(self,x_star):
        for i in range(self.y.shape[1]):
            return self.gpr[i].predict(x_star.reshape(1, -1))